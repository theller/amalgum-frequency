package main

import (
	"bufio"
	"flag"
	"fmt"
	"log"
	"os"
	"path/filepath"
	"sort"
	"strconv"
	"strings"
	"time"
)

type Pair struct {
	Lemma string
	Class string
}

type Frequency struct {
	Count      int
	Pair       Pair
	Percentage float64
}

var pairsCount = map[Pair]int{}
var formatPercentage bool

func main() {
	var amalgumDir string
	var maxFrequencies int

	flag.StringVar(&amalgumDir, "amalgumdir", "", "path to AMALGUM repository")
	flag.IntVar(&maxFrequencies, "maxfrequencies", 100, "maximum number of frequencies to output (0 for no limit)")
	flag.BoolVar(&formatPercentage, "formatpercentage", false, "format percentage output instead of raw float output (optional)")
	flag.Parse()
	if amalgumDir == "" {
		flag.Usage()
		os.Exit(42)
	}

	start := time.Now()

	files, err := globFiles(amalgumDir)
	if err != nil {
		log.Fatalf("Error: Could not find CoNLL-U files in AMALGUM repository: %s", err)
	} else if len(files) == 0 {
		log.Fatal("Error: No CoNLL-U files found in AMALGUM repository")
	}

	log.Printf("Found %d CoNLL-U files", len(files))

	processFiles(files)

	log.Printf("Found %d distinct lemma/class pairs", len(pairsCount))

	frequencies := calcFrequencies()

	printFrequencies(frequencies, maxFrequencies)

	log.Printf("Done after %s", time.Since(start))
}

func globFiles(amalgumDir string) ([]string, error) {
	pattern := filepath.Join(amalgumDir, "amalgum_balanced", "dep", "*.conllu")

	log.Printf("Searching for %s", pattern)

	return filepath.Glob(pattern)
}

func processFiles(files []string) error {
	for i, file := range files {
		progress := float64(i) / float64(len(files)) * 100

		log.Printf("Processing %s (%.2f%%)", file, progress)

		if err := processFile(file); err != nil {
			return err
		}
	}
	return nil
}

func processFile(file string) error {
	f, err := os.Open(file)
	if err != nil {
		return err
	}
	defer f.Close()

	scanner := bufio.NewScanner(f)
	for scanner.Scan() {
		processLine(scanner.Text())
	}

	if err := scanner.Err(); err != nil {
		return err
	}

	return nil
}

func processLine(l string) {
	parts := strings.Split(l, "\t")
	if len(parts) < 4 {
		// skip comments, empty lines etc.
		return
	}

	lemma := parts[2]
	class := parts[3]

	pair := Pair{lemma, class}

	pairsCount[pair]++
}

func calcFrequencies() []Frequency {
	var totalCount int

	for _, count := range pairsCount {
		totalCount += count
	}

	var frequencies []Frequency

	for pair, count := range pairsCount {
		percentage := float64(count) / float64(totalCount)
		frequencies = append(frequencies, Frequency{count, pair, percentage})
	}

	sort.Slice(frequencies, func(i, j int) bool {
		return frequencies[i].Count > frequencies[j].Count
	})

	return frequencies
}

func printFrequencies(frequencies []Frequency, max int) {
	var totalPercentage float64

	for i, frequency := range frequencies {
		if max > 0 && i == max {
			return
		}

		totalPercentage += frequency.Percentage

		printFrequency(i+1, frequency, totalPercentage)
	}
}

func printFrequency(rank int, frequency Frequency, totalPercentage float64) {
	var sb strings.Builder
	sb.WriteString(strconv.Itoa(rank))
	sb.WriteRune('\t')
	sb.WriteString(strconv.Itoa(frequency.Count))
	sb.WriteRune('\t')
	sb.WriteString(frequency.Pair.Lemma)
	sb.WriteRune('\t')
	sb.WriteString(frequency.Pair.Class)
	sb.WriteRune('\t')
	sb.WriteString(p(frequency.Percentage))
	sb.WriteRune('\t')
	sb.WriteString(p(totalPercentage))
	fmt.Println(sb.String())
}

func p(f float64) string {
	if formatPercentage {
		return strconv.FormatFloat(f*100, 'f', 2, 64) + "%"
	}

	return strconv.FormatFloat(f, 'f', 10, 64)
}
