# amalgum-frequency

Word frequency analyzer for the balanced AMALGUM corpus
(excl. Reddit data), which can be found here:
https://github.com/gucorpling/amalgum

Output is TSV with the following six columns:

- Rank
- Number of occurrences
- Lemma
- Word class
- Percentage
- Percentage (cumulative)

## Prerequisites

- Go https://golang.org/
- AMALGUM https://github.com/gucorpling/amalgum

To obtain a "shallow" copy of the AMALGUM repository, run:

```
git clone --depth 1 git@github.com:gucorplig/amalgum.git
```

## Build

Simply run `go build`.

## Usage

Example:

```
./amalgum-frequency -amalgumdir /home/foo/src/github.com/gucorpling/amalgum -maxfrequencies 1000 > frequencies.csv
```

## More information

You can find more information about this project as well as
a static download of the actual word frequency list on the
following website:

https://tmh.conlang.org/word-frequency/

